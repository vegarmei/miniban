#!/bin/bash

#Henter ut den nyeste linjen av ssh eventloggen om den inneholder strengen Failed
LINJE=$(journalctl -u ssh | tail -n 1 | grep Failed)

#Tømmer dokumentet failedIP, som brukes for å midlertidig lagre alle ipadresser som har failet på login mindre enn 3 ganger
echo "" > failedIP.txt

./unban.sh &

while true; do
	LINJEB=$(journalctl -u ssh | tail -n 1 | grep Failed)
#Sjekker om den nyeste linjen som inneholder Failed har endret seg
	if [[ "$LINJE" != "$LINJEB" ]]
	then
#Sjekker så at den linjen ikke er tom
		if [[ "$LINJEB" != "" ]]
		then
#Lagrer så den nyeste ip-adressen som har failet på å logge inn i variabelen IP, og lagrer den som en linje i filen failedIP.txt
			IP=$(echo $LINJEB | grep -oP '(\d{1,3}\.){3}\d{1,3}')
			echo $IP >> failedIP.txt
			echo "$IP"

#Deklarerer variabelen REPEATEDIP som ip-adresser som gjentas mer enn to ganger i failedIP.txt, denne komandoen vil printe alle ip-adresser som gjentas mer enn to ganger, men ettersom denne sjekken kjøres etter hver nye ip som blir lagt inn i filen og alle ip-adresser som gjentas mer enn to ganger blir fjernet i det de blir sendt til ban.sh
			REPEATEDIP=$(awk '{a[$0]++}END{for(i in a){if(a[i] > 2){print i}}}' failedIP.txt)
#Sjekker om REPEATEDIP har noe innhold, altså om det er en ip-adresse som har forsøkt å logge inn 3 ganger
			if [[ "$REPEATEDIP" != "" ]] && grep -vq "^$IP$" miniban.whitelist
			then
#Kjører ban scriptet med ip-adressen, og printer outputen i terminalen
				echo $(./ban.sh $IP)
#Fjerner alle linjene med ip-adressen i seg.
				grep -v $IP failedIP.txt > failedIP.txt.tmp && mv failedIP.txt.tmp failedIP.txt

			fi
		fi
#Dersom den nyeste linjen har endret seg settes LINJE til den nåverende ip-adressen slik at man sammenligner med den nyeste endrede linjen i neste runde av loopen.
		LINJE=$LINJEB
	else
	:
	fi
done

trap 'kill $(jobs -p)' EXIT
