#! /bin/bash

IPADDR="$1"

if [[ "$IPADDR" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then

	echo "Banning $IPADDR"

	sudo iptables -A INPUT -s $IPADDR -j REJECT
	sudo /sbin/iptables-save

	echo "$IPADDR",$(date +%s) >> miniban.db

else
	echo "usage: $0 <ip address>" 1>&2 # Redirect to stderr
	exit
fi

