#! /bin/bash

while true; do
	TIME=$(date "+%s")

awk -F',' '{print $2}' miniban.db | while read INPUT ; do

	IPADDR=$(echo $INPUT | cut -d "," -f1)
	BANTIME=$(echo $INPUT | cut -d "," -f2)
	DIFF=$(( TIME-BANTIME ))

	if [[ $DIFF -gt 600 ]]; then

		echo "Unbanning $IPADDR"

		sudo iptables -D INPUT -s $IPADDR -j REJECT
		sudo /sbin/iptables-save

		grep -v "^$INPUT$" > miniban.tmp
		mv miniban.tmp miniban.db
	fi
	sleep 30s
done < miniban.db

done

