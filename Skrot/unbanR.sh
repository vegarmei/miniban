#! /bin/bash

IPADDR="$1"

if [[ "$IPADDR" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then

	echo "Unbanning $IPADDR"

	sudo iptables -D INPUT -s $IPADDR -j REJECT
	sudo /sbin/iptables-save

	sed "/$IPADDR/d" miniban.db > miniban.db

else
	echo "usage: $0 <ip address>" 1>&2 # Redirect to stderr
	exit
fi

